package main

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"text/template"
)

const port = ":5050"

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/GetImplementedMethods", getImplementedMethodsHandler).Methods(http.MethodPost)

	if err := http.ListenAndServe(port, router); err != nil {
		log.Fatal(err)
	}
}

type soapRQ struct {
	XMLName   xml.Name `xml:"Envelope"`
	XMLNsSoap string   `xml:"xmlns:soap,attr"`
	XMLNsXSI  string   `xml:"xmlns:xsi,attr"`
	XMLNsXSD  string   `xml:"xmlns:xsd,attr"`
	Body      soapBody
}

type soapBody struct {
	XMLName  xml.Name `xml:"Body"`
	TickedId string   `xml:"TickedId"`
}

type GetImplementedMethodsResponse struct {
	Status Status `xml:"Status"`
}

type Status struct {
	Code     string   `xml:"Code"`
	Method   []string `xml:"Result"`
	TickedId string   `xml:"TickedId"`
}

func getImplementedMethodsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/xml; charset=utf-8")
	w.Header().Set("SOAPAction", "127.0.0.1:5050/GetImplementMethods")

	var sr soapRQ
	err := xml.NewDecoder(r.Body).Decode(&sr)
	if err != nil {
		fmt.Fprintf(w, err.Error())
		return
	}

	b, err := SoapFormat(GetImplementedMethods(sr.Body.TickedId))
	if err != nil {
		fmt.Fprintf(w, err.Error())
		return
	}

	fmt.Fprintf(w, b.String())
}

func GetImplementedMethods(tickedID string) GetImplementedMethodsResponse {
	return GetImplementedMethodsResponse{
		Status: Status{
			Code: "0",
			Method: []string{
				"GetDocumentList",
				"RegisterRequestToPublishSourceDocs",
				"RegisterAvailableFile",
				"UpdateFileProcessingStatus",
				"GetDatamartRequestsToPublishDocuments",
				"RegisterRequestToDocument",
			},
			TickedId: tickedID,
		},
	}
}

var soapTemplate = `
	<soapenv:Envelope
		xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
		xmlns:api="http://soapdummies.com/api">
	<soapenv:Header/>
		<soapenv:Body>
		<api:Command
		xmlns="http://soapdummies.com/api">
		<api:Body>
		<SOAPDummy schemaVersion="3.0"
		xmlns="http://soapdummies.com/products/request">
			{{.SoapModel}}
		</SOAPDummy>
		</api:Body>
	</api:Command>
	</soapenv:Body>
	</soapenv:Envelope>
`

func SoapFormat(m interface{}) (bytes.Buffer, error) {
	bb, err := xml.Marshal(m)
	if err != nil {
		return bytes.Buffer{}, err
	}

	ut, err := template.New("data").Parse(soapTemplate)
	if err != nil {
		return bytes.Buffer{}, err
	}

	var tpl bytes.Buffer

	err = ut.Execute(&tpl, struct{ SoapModel string }{string(bb)})
	if err != nil {
		return bytes.Buffer{}, err
	}

	return tpl, nil
}
