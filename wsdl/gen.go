package main

import (
	"bytes"
	"log"
	"os"
	"text/template"
)

//go:generate go run gen.go

func main() {
	generateWSDLFile()
}

func generateWSDLFile() {
	if r := recover(); r != nil {
		log.Fatal(r)
		return
	}

	f, err := os.Create("gnivc.wsdl")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	s, err := WSDLGenerateString()
	if err != nil {
		panic(err)
	}

	f.Write([]byte(s))
}

var generateTemplate = `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<wsdl:definitions xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
                  xmlns:tns="http://www.cleverbuilder.com/BookService/"
                  xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
                  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                  name="{{.Name}}"
                  targetNamespace="http://www.cleverbuilder.com/BookService/">
<wsdl:types>
<xsd:schema targetNamespace="{{.TargetNameSpace}}">
  {{.Element}}
</xsd:schema>
</wsdl:types>
  {{.Request}}
  {{.Port}}
  {{.Binding}}
  {{.Operation}}
</wsdl:definitions>
`

type WSDLGenerate struct {
	TNS             string
	Name            string
	TargetNameSpace string
	Element         string
	Request         string
	Port            string
	Binding         string
	Operation       string
}

func WSDLGenerateString() (string, error) {

	element, err := elementGenerate("ElementTest", map[string]string{
		"TickedId": "string",
	})
	if err != nil {
		return "", err
	}

	request, err := requestGenerate("RequestTest", map[string]string{
		"ElementTest": "ElementTest",
	})
	if err != nil {
		return "", err
	}

	// port
	service, err := serviceGenerate("ServiceTest", []operation{
		{
			Name: "FirstOperation",
			Messages: map[string]string{
				"RequestTest": "input",
			},
		},
	})
	if err != nil {
		return "", err
	}

	binding, err := bindingGenerate("BindingTest", "OperationName", "ServiceTest", "Action")
	if err != nil {
		return "", err
	}

	o, err := operationGenerate("OperationTest", "BindingTest", "BindingTestName", "Location")
	if err != nil {
		return "", err
	}

	w := WSDLGenerate{
		Name:            "GNIVCService",
		TNS:             "http://127.0.0.1:5050/GNIVCService",
		TargetNameSpace: "TargetTest",
		Element:         element,
		Request:         request,
		Port:            service,
		Binding:         binding,
		Operation:       o,
	}

	tt, err := generateWSDLTemplate("element", generateTemplate, w)
	if err != nil {
		return "", err
	}

	return tt.String(), nil
}

func elementGenerate(name string, elements map[string]string) (string, error) {
	t := `
<xsd:element name="{{.Name}}">
<xsd:complexType>
  <xsd:sequence>
{{range  $element, $type := .Elements}} 
	<xsd:element name="{{$element}}" type="xsd:{{$type}}" minOccurs="0"/>
{{end}}
  </xsd:sequence>
</xsd:complexType>
</xsd:element>
`

	tt, err := generateWSDLTemplate("element", t, struct {
		Name     string
		Elements map[string]string
	}{
		Name:     name,
		Elements: elements,
	})
	if err != nil {
		return "", err
	}

	return tt.String(), nil
}

func requestGenerate(name string, p map[string]string) (string, error) {
	t := `
<wsdl:message name="{{.Name}}">
{{range  $element, $name := .Parameters}} 
    <wsdl:part element="tns:{{$element}}" name="{{$name}}"/>
{{end}}
</wsdl:message>
`
	tt, err := generateWSDLTemplate("element", t, struct {
		Name       string
		Parameters map[string]string
	}{
		Name:       name,
		Parameters: p,
	})
	if err != nil {
		return "", err
	}

	return tt.String(), nil
}

type operation struct {
	Name     string
	Messages map[string]string
}

func serviceGenerate(name string, o []operation) (string, error) {
	t := `
<wsdl:portType name="{{.PortName}}">
{{range .Operations}} 
<wsdl:operation name="{{.Name}}">
	{{range $message, $operation := .Messages}}  
		   <wsdl:{{$operation}} message="tns:{{$message}}"/>
	{{end}}
</wsdl:operation>
{{end}}
</wsdl:portType>
`
	tt, err := generateWSDLTemplate("element", t, struct {
		PortName   string
		Operations []operation
	}{
		PortName:   name,
		Operations: o,
	})
	if err != nil {
		return "", err
	}

	return tt.String(), nil
}

func bindingGenerate(name, operation, service, action string) (string, error) {
	t := `
<wsdl:binding name="{{.Name}}" type="tns:{{.Service}}">
<soap:binding style="document"
			  transport="http://schemas.xmlsoap.org/soap/http"/>
<wsdl:operation name="{{.Operation}}">
 <soap:operation soapAction="{{.Action}}"/>
	<wsdl:input>
	<soap:body use="literal"/>
	</wsdl:input>
	<wsdl:output>
	<soap:body use="literal"/>
	</wsdl:output>
</wsdl:operation>
</wsdl:binding>
`
	tt, err := generateWSDLTemplate("element", t, struct {
		Name      string
		Service   string
		Operation string
		Action    string
	}{
		Name:      name,
		Service:   service,
		Operation: operation,
		Action:    action,
	})
	if err != nil {
		return "", err
	}

	return tt.String(), nil
}

func operationGenerate(name, binding, bindingName, location string) (string, error) {
	t := `
<wsdl:service name="{{.Name}}">
    <wsdl:port binding="tns:{{.Binding}}" name="{{.BindingName}}">
      <soap:address location="{{.Location}}"/>
    </wsdl:port>
</wsdl:service>
`
	tt, err := generateWSDLTemplate("element", t, struct {
		Name        string
		Binding     string
		BindingName string
		Location    string
	}{
		Name:        name,
		Binding:     binding,
		BindingName: bindingName,
		Location:    location,
	})
	if err != nil {
		return "", err
	}

	return tt.String(), nil
}

func generateWSDLTemplate(name string, t string, v interface{}) (bytes.Buffer, error) {
	ut, err := template.New(name).Parse(t)
	if err != nil {
		return bytes.Buffer{}, err
	}

	var tpl bytes.Buffer
	err = ut.Execute(&tpl, v)
	if err != nil {
		return tpl, err
	}

	return tpl, nil
}
