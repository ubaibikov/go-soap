## Request to 127.0.0.1:5050/GetImplementedMethods endpoint:

#### POST
````
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Body>
<TickedId>test</TickedId>
</soap:Body>
</soap:Envelope>
````

## Response to 127.0.0.1:5050/GetImplementedMethods endpoint:


````
<soapenv:Envelope
		xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
		xmlns:api="http://soapdummies.com/api">
    <soapenv:Header/>
    <soapenv:Body>
        <api:Command
		xmlns="http://soapdummies.com/api">
            <api:Body>
                <SOAPDummy schemaVersion="3.0"
		xmlns="http://soapdummies.com/products/request">
                    <GetImplementedMethodsResponse>
                        <Status>
                            <Code>0</Code>
                            <Result>GetDocumentList</Result>
                            <Result>RegisterRequestToPublishSourceDocs</Result>
                            <Result>RegisterAvailableFile</Result>
                            <Result>UpdateFileProcessingStatus</Result>
                            <Result>GetDatamartRequestsToPublishDocuments</Result>
                            <Result>RegisterRequestToDocument</Result>
                            <TickedId>test</TickedId>
                        </Status>
                    </GetImplementedMethodsResponse>
                </SOAPDummy>
            </api:Body>
        </api:Command>
    </soapenv:Body>
</soapenv:Envelope>
````
